# TP1-Linux : (re)Familiaration avec un système GNU/Linux

## 0. Préparation de la machine

```
[adrien@node1 ~]$ ip a
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:29:c4:c9 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 86234sec preferred_lft 86234sec
    inet6 fe80::a00:27ff:fe29:c4c9/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
[...]
```
La carte réseau dédié est la carte enp0s3


```
[adrien@node1 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
[...]
```
La route par défaut est la premiére route, elle utilise la carte enp0s3

---
Carte réseau dédiée et Ip de la premiere machine : 
```
[adrien@node1 ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:59:df:00 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.11/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe59:df00/64 scope link
       valid_lft forever preferred_lft forever
```
L'ip de la premiere machine est 10.101.1.11/24

Carte réseau dédiée et Ip de la deuxieme machine :
```
[adrien@node2 ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f5:dc:08 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.12/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fef5:dc08/64 scope link
       valid_lft forever preferred_lft forever
```
L'ip de la deuxieme machine est 10.101.1.12/24

Les deux machines possedent leur ip statique sur l'interface host only, qui est l'interface enp0s8
![](https://i.imgur.com/BQGRzrG.png)


---

Nom de la premiere machine : 
```
[adrien@node1 ~]$ hostname
node1.tp1.b2
```
Nom de la deuxieme machine : 
```
[adrien@node2 ~]$ hostname
node2.tp1.b2
```
---

Dig ynov.com apres avoir changé le DNS en 1.1.1.1

```
[adrien@node1 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 46344
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               988     IN      A       92.243.16.143

;; Query time: 21 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Wed Sep 22 11:54:54 CEST 2021
;; MSG SIZE  rcvd: 53
```
La ligne qui contient l'adresse IP du serveur qui correspond au nom demandé : 
```
;; ANSWER SECTION:
ynov.com.               988     IN      A       92.243.16.143
```
La ligne qui contient l'adresse IP du serveur qui vous a répondu
```
;; SERVER: 1.1.1.1#53(1.1.1.1)
```
---
 
Ping de node1 vers node2 avec son nom : 
```
[adrien@node1 ~]$ ping node2.tp1.b2
PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.529 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.675 ms
^C
--- node2.tp1.b2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 0.529/0.602/0.675/0.073 ms
```
Ping de node2 vers node1 avec son nom :
```
[adrien@node2 ~]$ ping node1.tp1.b2
PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=1 ttl=64 time=0.393 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=2 ttl=64 time=0.622 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=3 ttl=64 time=0.598 ms
^C
--- node1.tp1.b2 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2033ms
rtt min/avg/max/mdev = 0.393/0.537/0.622/0.106 ms
```

---
Regles du firewall : 
```
[adrien@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
C'est le meme sur les 2 machines


## I. Utilisateurs

### 1. Création et configuration
Création d'un utilisateurs 

```
[adrien@node1 ~]$ sudo useradd toto -m -s /bin/sh -u 2001
[adrien@node1 ~]$ sudo passwd toto
[...]
[adrien@node1 ~]$ ls /home/
adrien  Adrien  toto
```

Le groupe admins à tous les droit grace a cette ligne : 
```%admins ALL=(ALL) ALL```

toto est dans le groupe admins : 
```
[toto@node1 ~]$ groups toto
toto : toto admins
```

### 2. SSH

Connection en ssh sans avoir besoin de mot de passe : 
```
C:\Users\adrie>ssh toto@10.101.1.11
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Sun Sep 26 16:11:20 2021 from 10.101.1.1
[toto@node1 ~]$
```

## II. Partitionnement

Création des logical volumes de 1 Go

```
[toto@node1 ~]$ sudo lvs
  LV        VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  last_data data -wi-a-----   5.99g
  root      rl   -wi-ao----  <6.20g
  swap      rl   -wi-ao---- 820.00m
[toto@node1 ~]$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/data/last_data
  LV Name                last_data
  VG Name                data
  LV UUID                8EOuA8-cmtQ-R3VZ-cVNN-1CKe-WZz9-qpKbEe
  LV Write Access        read/write
  LV Creation host, time node1.tp1.b2, 2021-09-26 16:44:04 +0200
  LV Status              available
  # open                 0
  LV Size                5.99 GiB
  Current LE             1534
  Segments               2
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2

  --- Logical volume ---
  LV Path                /dev/rl/swap
  LV Name                swap
  VG Name                rl
  LV UUID                rydAj3-f2IZ-mUst-eD3V-kABX-UeQe-O8TcQD
  LV Write Access        read/write
  LV Creation host, time bastion-ovh1fr.auvence.co, 2021-09-15 10:47:59 +0200
  LV Status              available
  # open                 2
  LV Size                820.00 MiB
  Current LE             205
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:1

  --- Logical volume ---
  LV Path                /dev/rl/root
  LV Name                root
  VG Name                rl
  LV UUID                F0sIoX-aGf9-Brg5-cOZi-BmLt-ygoN-xdJ4zP
  LV Write Access        read/write
  LV Creation host, time bastion-ovh1fr.auvence.co, 2021-09-15 10:48:00 +0200
  LV Status              available
  # open                 1
  LV Size                <6.20 GiB
  Current LE             1586
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:0

```
Les partitions sont partitionés et montés : 
```
[toto@node1 ~]$  df -h
Filesystem                      Size  Used Avail Use% Mounted on
devtmpfs                        891M     0  891M   0% /dev
tmpfs                           909M     0  909M   0% /dev/shm
tmpfs                           909M  8.5M  901M   1% /run
tmpfs                           909M     0  909M   0% /sys/fs/cgroup
/dev/mapper/rl-root             6.2G  2.1G  4.2G  33% /
/dev/sda1                      1014M  240M  775M  24% /boot
tmpfs                           182M     0  182M   0% /run/user/2001
/dev/mapper/data-ma_data_frer   976M  2.6M  907M   1% /mnt/part1
/dev/mapper/data-ma_data_frer2  976M  2.6M  907M   1% /mnt/part2
/dev/mapper/data-ma_data_frer3  976M  2.6M  907M   1% /mnt/part3
```

Les partitions sont montés automatiquement 
```
[toto@node1 ~]$ sudo sudo umount /mnt/part1
[toto@node1 ~]$ sudo sudo umount /mnt/part2
[toto@node1 ~]$ sudo sudo umount /mnt/part3
[toto@node1 ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /mnt/part1 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part1               : successfully mounted
mount: /mnt/part2 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part2               : successfully mounted
mount: /mnt/part3 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part3               : successfully mounted
```

 ## III. Gestion de services
 
 ### 1. Interaction avec un service existant
 
 Unité firewalld
 
```
[toto@node1 ~]$ systemctl is-active firewalld
active
[toto@node1 ~]$ systemctl is-enabled firewalld
enabled
```

### 2. Création de service

#### A. Unité simpliste

Test de l'accés au serveur web
```
[toto@node1 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```

#### B. Modification de l'unité
création d'un tilisateurs web : 
```
[toto@node1 ~]$ sudo useradd web
```
On vérifie le bon fonctionnement apres l'ajout de WorkingDirectory= et User= , et le création du nouveau dossier
```
[web@node1 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="hello.txt">hello.txt</a></li>
</ul>
<hr>
</body>
</html>
```