# TP2 pt. 1 : Gestion de service

## 1. Installation

Installer le serveur Apache

```
[adrien@web ~]$ sudo dnf install httpd
[...]
```




### TEST
vérifier que le service est démarré
```
[adrien@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-10-05 14:53:31 CEST; 12min ago
     Docs: man:httpd.service(8)
 Main PID: 2148 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 11398)
   Memory: 24.7M
   CGroup: /system.slice/httpd.service
           ├─2148 /usr/sbin/httpd -DFOREGROUND
           ├─2149 /usr/sbin/httpd -DFOREGROUND
           ├─2150 /usr/sbin/httpd -DFOREGROUND
           ├─2151 /usr/sbin/httpd -DFOREGROUND
           └─2152 /usr/sbin/httpd -DFOREGROUND

Oct 05 14:53:31 localhost.localdomain systemd[1]: Starting The Apache HTTP Server...
Oct 05 14:53:31 localhost.localdomain httpd[2148]: AH00558: httpd: Could not reliably determine the server's fully qualified domain name, using localhost.localdomain. Set the 'ServerName' directive globally to suppress this message
Oct 05 14:53:31 localhost.localdomain systemd[1]: Started The Apache HTTP Server.
Oct 05 14:53:31 localhost.localdomain httpd[2148]: Server configured, listening on: port 80
```

vérifier qu'il est configuré pour démarrer automatiquement
```
[adrien@web ~]$ sudo systemctl is-enabled httpd
enabled
```

vérifier avec une commande curl localhost que vous joignez votre serveur web localement
```
[adrien@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, 
[...]
  </body>
</html>
```

vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web;
![](https://i.imgur.com/lWCtndX.png)

## 2. Avancer vers la maîtrise du service

### Le service Apache...


donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume
```sudo systemctl enable httpd```

prouvez avec une commande qu'actuellement, le service est paramétré pour démarrer quand la machine s'allume
```
[adrien@web ~]$ sudo systemctl is-enabled httpd
enabled
```

affichez le contenu du fichier httpd.service qui contient la définition du service Apache
```
[adrien@web ~]$ cat /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

### Déterminer sous quel utilisateur tourne le processus Apache

mettez en évidence la ligne dans le fichier de conf principal d'Apache (httpd.conf) qui définit quel user est utilisé
```
[adrien@web ~]$ cat /etc/httpd/conf/httpd.conf  
[...]
User apache
[...]
```

utilisez la commande ps -ef pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf
```apache      2149    2148  0 14:53 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2150    2148  0 14:53 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache      2151    2148  0 14:53 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache      2152    2148  0 14:53 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND```
```
vérifiez avec un ls -al le dossier du site que tout son contenu est accessible en lecture à l'utilisateur mentionné dans le fichier de conf
```
[adrien@web ~]$ ls -al /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Oct  5 14:53 .
drwxr-xr-x. 98 root root 4096 Oct  6 11:14 ..
-rw-r--r--.  1 root root 7621 Jun 11 17:23 index.html
```
-rw-r--**r**-- le dernier "r" nous donne l'informations que tous le monde peut lire le fichier, donc l'utilisateur apache peut lire le fichier

### Changer l'utilisateur utilisé par Apache
modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur
```User web```

redémarrez Apache

utilisez une commande ps pour vérifier que le changement a pris effet


```
web         2748    2746  0 16:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web         2749    2746  0 16:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web         2750    2746  0 16:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web         2751    2746  0 16:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```
### Faites en sorte que Apache tourne sur un autre port
modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port de votre choix
```
[adrien@web ~]$ cat /etc/httpd/conf/httpd.conf
[...]
Listen 81
[...]
```

ouvrez un nouveau port firewall, et fermez l'ancien
```
[adrien@web ~]$ sudo firewall-cmd --add-port=81/tcp --permanent
success
[adrien@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
```
redémarrez Apache

```[adrien@web ~]$ sudo systemctl restart httpd ```

prouvez avec une commande ss que Apache tourne bien sur le nouveau port choisi
```
LISTEN          0               128                                  *:81                                  *:*              users:(("httpd",pid=2995,fd=4),("httpd",pid=2994,fd=4),("httpd",pid=2993,fd=4),("httpd",pid=2990,fd=4))
```
**:81**

vérifiez avec curl en local que vous pouvez joindre Apache sur le nouveau port
```
[adrien@web ~]$ curl localhost:81
<!doctype html>
<html>
  <head>
[...]
  </body>
</html>
```

vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port
![](https://i.imgur.com/bI210tq.png)

## II. Une stack web plus avancée

### 2. Setup

#### A. Serveur Web et NextCloud
Install du serveur Web et de NextCloud sur web.tp2.linux

Installation des différents packages sur le serveur web :
```
[adrien@web ~]$ sudo dnf install epel-release
[...]
[adrien@web ~]$ sudo dnf update
[...]
[adrien@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
[...]
[adrien@web ~]$ sudo dnf module enable php:remi-7.4
[...]
[adrien@web ~]$ dnf install httpd vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
[...]
```

Création des dossiers sites-available,sites-enabled et sub-domains : 
```
[adrien@web ~]$ mkdir /etc/httpd/sites-available
[adrien@web ~]$ mkdir /etc/httpd/sites-enabled
[adrien@web ~]$ mkdir /var/www/sub-domains/
```

Ajout dans la ligne `Include /etc/httpd/sites-enabled` a la fin de httpd.conf
(l'oublie de cette ligne ma fait perdre 1h30...)
![Alt Text](https://media.giphy.com/media/7GnnDNH1ocZwc/giphy.gif)

```
[adrien@web ~]$ vi /etc/httpd/conf/httpd.conf
```

Création du fichier web.tp2.linux 
```
[adrien@web ~]$ sudo vi /etc/httpd/sites-available/web.tp2.linux
<VirtualHost *:80>
  DocumentRoot /var/www/sub-domains/web.tp2.linux/html/
  ServerName  web.tp2.linux

  <Directory /var/www/sub-domains/web.tp2.linux/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>

```

On créé un lien dans vers enabled vers notre fichier
```
[adrien@web ~]$ sudo ln -s /etc/httpd/sites-available/web.tp2.linux /etc/httpd/sites-enabled/
```

On créé le repo de notre site
```
[adrien@web ~]$ sudo mkdir -p /var/www/sub-domains/web.tp2.linux/html
```

On récupére notre timezone, ici c'est Europe/Paris
```
[adrien@web ~]$ timedatectl
               Local time: Sun 2021-10-10 19:37:41 CEST
           Universal time: Sun 2021-10-10 17:37:41 UTC
                 RTC time: Sun 2021-10-10 17:37:41
                Time zone: Europe/Paris (CEST, +0200)
System clock synchronized: no
              NTP service: inactive
          RTC in local TZ: no
```

On modifie le ficher php.ini pour y ajouter notre 
```
[adrien@web ~]$ sudo vi /etc/opt/remi/php74/php.ini
[...]
;date.timezone = Europe/Paris
[...]
```

On télécharge un zip contenant les fichiers nextcloud puis on l'unzip
```
[adrien@web ~]$ sudo wget https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
[...]
[adrien@web ~]$ sudo unzip nextcloud-21.0.1.zip
[...]
```

Je deplace le contenu de nextcloud dans mon repo et je change le proprietaire du fichier pour apache 
```
[adrien@web ~]$ sudo cp -Rf * /var/www/sub-domains/web.tp2.linux/html/
[adrien@web ~]$ sudo cp chown -Rf apache.apache /var/www/sub-domains/web.tp2.linux/html
```

je deplace le dossier data dans le DocumentRoot :
```
mv /var/www/sub-domains/web.tp2.linux/html/data /var/www/sub-domains/web.tp2.linux/
```

Je modifie le fichier web.tp2.linux pour changer la route vers le dossier nextcloud

```
[adrien@web ~]$ sudo vi /etc/httpd/sites-available/web.tp2.linux
[...]
  DocumentRoot /var/www/sub-domains/web.tp2.linux/html/nextcloud
[...]
  <Directory /var/www/sub-domains/web.tp2.linux/html/nextcloud>
[...]
```
Je redemare le service httpd et je peux accédés au site depuis un navigateur avec l'adresse http://10.102.1.11
![](https://i.imgur.com/WVX4SMb.png)

### B. Base de données
Installation du package mariadb sur la machine db
```
[adrien@db ~]$ sudo dnf install httpd mariadb-server
```

Je modifie le mot de passe root de mariadb
```
sudo mysql -u root
```
Je créé un utilisateurs nextcloud avec pour mot de passe meow
```
CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
```
Je créé la base de donnée
```
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
```
Je donne tous les droits à l’utilisateur nextcloud sur les tables 
```
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
```
activation des privilèges :
```
FLUSH PRIVILEGES;
```

J'ouvre le port 3306 pour permettre la connexions 
```
[adrien@db ~]$ firewall-cmd --add-port=3306/tcp --permanent
[adrien@db ~]$ firewall-cmd --add-port=3306/udp --permanent
[adrien@db ~]$ firewall-cmd --add-port=3306/tcp 
[adrien@db ~]$ firewall-cmd --add-port=3306/udp 
```
Connexion depuis web à la base de données
```

[adrien@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 9
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```
On affiche la base de données 
```

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.003 sec)
```
On selectionne et affichage une table spécifique :
```

MariaDB [information_schema]> USE information_schema;
Database changed
MariaDB [information_schema]> SHOW TABLES;
+---------------------------------------+
| Tables_in_information_schema          |
+---------------------------------------+
| ALL_PLUGINS                           |
| APPLICABLE_ROLES                      |
| CHARACTER_SETS                        |
| CHECK_CONSTRAINTS                     |
| COLLATIONS                            |
| COLLATION_CHARACTER_SET_APPLICABILITY |
| COLUMNS                               |
| COLUMN_PRIVILEGES                     |
| ENABLED_ROLES                         |
| ENGINES                               |
| EVENTS                                |
| FILES                                 |
| GLOBAL_STATUS                         |
| GLOBAL_VARIABLES                      |
| KEY_CACHES                            |
| KEY_COLUMN_USAGE                      |
| PARAMETERS                            |
| PARTITIONS                            |
| PLUGINS                               |
| PROCESSLIST                           |
| PROFILING                             |
| REFERENTIAL_CONSTRAINTS               |
| ROUTINES                              |
| SCHEMATA                              |
| SCHEMA_PRIVILEGES                     |
| SESSION_STATUS                        |
| SESSION_VARIABLES                     |
| STATISTICS                            |
| SYSTEM_VARIABLES                      |
| TABLES                                |
| TABLESPACES                           |
| TABLE_CONSTRAINTS                     |
| TABLE_PRIVILEGES                      |
| TRIGGERS                              |
| USER_PRIVILEGES                       |
| VIEWS                                 |
| GEOMETRY_COLUMNS                      |
| SPATIAL_REF_SYS                       |
| CLIENT_STATISTICS                     |
| INDEX_STATISTICS                      |
| INNODB_SYS_DATAFILES                  |
| USER_STATISTICS                       |
| INNODB_SYS_TABLESTATS                 |
| INNODB_LOCKS                          |
| INNODB_MUTEXES                        |
| INNODB_CMPMEM                         |
| INNODB_CMP_PER_INDEX                  |
| INNODB_CMP                            |
| INNODB_FT_DELETED                     |
| INNODB_CMP_RESET                      |
| INNODB_LOCK_WAITS                     |
| TABLE_STATISTICS                      |
| INNODB_TABLESPACES_ENCRYPTION         |
| INNODB_BUFFER_PAGE_LRU                |
| INNODB_SYS_FIELDS                     |
| INNODB_CMPMEM_RESET                   |
| INNODB_SYS_COLUMNS                    |
| INNODB_FT_INDEX_TABLE                 |
| INNODB_CMP_PER_INDEX_RESET            |
| user_variables                        |
| INNODB_FT_INDEX_CACHE                 |
| INNODB_SYS_FOREIGN_COLS               |
| INNODB_FT_BEING_DELETED               |
| INNODB_BUFFER_POOL_STATS              |
| INNODB_TRX                            |
| INNODB_SYS_FOREIGN                    |
| INNODB_SYS_TABLES                     |
| INNODB_FT_DEFAULT_STOPWORD            |
| INNODB_FT_CONFIG                      |
| INNODB_BUFFER_PAGE                    |
| INNODB_SYS_TABLESPACES                |
| INNODB_METRICS                        |
| INNODB_SYS_INDEXES                    |
| INNODB_SYS_VIRTUAL                    |
| INNODB_TABLESPACES_SCRUBBING          |
| INNODB_SYS_SEMAPHORE_WAITS            |
+---------------------------------------+
76 rows in set (0.001 sec)
```

### C. Finaliser l'installation de NextCloud

modifiez fichier hosts dans C:\WINDOWS\System32\drivers\etc\hosts et y ajouter la ligne 
`10.102.1.11      web.tp2.linux`

Je vais sur le site avec le lien http://web.tp2.linux
![](https://i.imgur.com/j7cCBtl.png)

Exploration de la base de données

```

MariaDB [(none)]> USE nextcloud;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [nextcloud]> SHOW TABLES;
+-----------------------------+
| Tables_in_nextcloud         |
+-----------------------------+
| oc_accounts                 |
| oc_accounts_data            |
| oc_activity                 |
| oc_activity_mq              |
| oc_addressbookchanges       |
| oc_addressbooks             |
| oc_appconfig                |
| oc_authtoken                |
| oc_bruteforce_attempts      |
| oc_calendar_invitations     |
| oc_calendar_reminders       |
| oc_calendar_resources       |
| oc_calendar_resources_md    |
| oc_calendar_rooms           |
| oc_calendar_rooms_md        |
| oc_calendarchanges          |
| oc_calendarobjects          |
| oc_calendarobjects_props    |
| oc_calendars                |
| oc_calendarsubscriptions    |
| oc_cards                    |
| oc_cards_properties         |
| oc_circles_circle           |
| oc_circles_event            |
| oc_circles_member           |
| oc_circles_membership       |
| oc_circles_mount            |
| oc_circles_mountpoint       |
| oc_circles_remote           |
| oc_circles_share_lock       |
| oc_circles_token            |
| oc_collres_accesscache      |
| oc_collres_collections      |
| oc_collres_resources        |
| oc_comments                 |
| oc_comments_read_markers    |
| oc_dav_cal_proxy            |
| oc_dav_shares               |
| oc_direct_edit              |
| oc_directlink               |
| oc_federated_reshares       |
| oc_file_locks               |
| oc_filecache                |
| oc_filecache_extended       |
| oc_files_trash              |
| oc_flow_checks              |
| oc_flow_operations          |
| oc_flow_operations_scope    |
| oc_group_admin              |
| oc_group_user               |
| oc_groups                   |
| oc_jobs                     |
| oc_known_users              |
| oc_login_flow_v2            |
| oc_mail_accounts            |
| oc_mail_aliases             |
| oc_mail_attachments         |
| oc_mail_classifiers         |
| oc_mail_coll_addresses      |
| oc_mail_mailboxes           |
| oc_mail_message_tags        |
| oc_mail_messages            |
| oc_mail_provisionings       |
| oc_mail_recipients          |
| oc_mail_tags                |
| oc_mail_trusted_senders     |
| oc_migrations               |
| oc_mimetypes                |
| oc_mounts                   |
| oc_notifications            |
| oc_notifications_pushhash   |
| oc_oauth2_access_tokens     |
| oc_oauth2_clients           |
| oc_preferences              |
| oc_privacy_admins           |
| oc_properties               |
| oc_ratelimit_entries        |
| oc_recent_contact           |
| oc_richdocuments_assets     |
| oc_richdocuments_direct     |
| oc_richdocuments_wopi       |
| oc_schedulingobjects        |
| oc_share                    |
| oc_share_external           |
| oc_storages                 |
| oc_storages_credentials     |
| oc_systemtag                |
| oc_systemtag_group          |
| oc_systemtag_object_mapping |
| oc_talk_attendees           |
| oc_talk_bridges             |
| oc_talk_commands            |
| oc_talk_internalsignaling   |
| oc_talk_rooms               |
| oc_talk_sessions            |
| oc_text_documents           |
| oc_text_sessions            |
| oc_text_steps               |
| oc_trusted_servers          |
| oc_twofactor_backupcodes    |
| oc_twofactor_providers      |
| oc_user_status              |
| oc_user_transfer_owner      |
| oc_users                    |
| oc_vcategory                |
| oc_vcategory_to_object      |
| oc_webauthn                 |
| oc_whats_new                |
+-----------------------------+
108 rows in set (0.002 sec)
```
108 tables ont était créés 


| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80         |             |
| `db.tp2.linux` | `10.102.1.12` | Base de données            | 3306         |   `10.102.1.11`          |
