#!/bin/bash
# Installation of a Jupyter Notebook server 
# adrien~ 20/11/21

#On créé un dossier dans le home de notre utilisateur
mkdir -p ~/jupyter

#On ce deplace dans le dossier
cd ~/jupyter

#On install les différents module python qui pourront etre utilisé sur notre jupyter 
pip3 install --upgrade
pip3 install jupyterlab
pip3 install matplotlib
pip3 install pandas

#On créé un dossier dans le home de notre utilisateur
mkdir -p ~/.jupyter

#On créé un fichier de config de notre jupyter
cat << EOF > ~/.jupyter/jupyter_notebook_config.py
c.NotebookApp.allow_remote_access = True
c.NotebookApp.ip = '0.0.0.0'
c.NotebookApp.open_browser = False
c.NotebookApp.port = 8080
c.NotebookApp.token = '$1'
EOF

#Fichier qui s'executeras au lancement de notre service et qui servira a créer des fichiers de conf
cat << EOF > ~/my-jupyterlab.te
module my-jupyterlab 1.0;

require {
        type init_t;
        type unconfined_exec_t;
        type user_home_t;
        type ephemeral_port_t;
        class tcp_socket { name_connect };
        class dir { create write read setattr rename };
        class file { append execute execute_no_trans ioctl map open read setattr create write relabelfrom rename };
        class lnk_file { getattr read };

}

#============= init_t ==============
allow init_t unconfined_exec_t:file { create execute open read };
allow init_t ephemeral_port_t:tcp_socket { name_connect };
allow init_t user_home_t:dir { create write read setattr rename };
allow init_t user_home_t:file { append execute execute_no_trans ioctl open read setattr map create write relabelfrom rename };
allow init_t user_home_t:lnk_file { getattr read };
EOF