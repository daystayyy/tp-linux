`Adrien Clamadieu-Tharaud &
Nicolas de Bengy`

Ce tp à pour but la mise en place d'un Notebook Jupyter (voici la documentation officiel: https://jupyter-notebook.readthedocs.io/en/stable/), ainsi qu'un reverse proxy à l'aide de nginx, et enfin nous avons rédigé un script d'installation pour le notebook.

# TP3 : Your own shiet

# Jupyter Notebook

## Installation du serveur web

### **➜ Manuellement**

### 1. Installation des packages et de pipenv

```bash
sudo dnf -y install python3

sudo pip3 install --upgrade pip

sudo pip3 install --upgrade setuptools

sudo pip3 install pipenv

echo "export PIPENV_VENV_IN_PROJECT=true" >> ~/.bashrc

source ~/.bashrc
```

### 2. Installation de Jupyter

```bash
mkdir -p ~/jupyter

cd ~/jupyter

pipenv --python 3

pipenv install

pipenv install jupyterlab

pipenv install matplotlib

pipenv install pandas

mkdir -p ~/.jupyter

# Vous pouvez ici choisir un token de connexion qui servira en quelque sorte de mot de passe: 🪐
cat << EOF > ~/.jupyter/jupyter_notebook_config.py
c.NotebookApp.allow_remote_access = True
c.NotebookApp.ip = '0.0.0.0'
c.NotebookApp.open_browser = False
c.NotebookApp.port = 8080
c.NotebookApp.token = 🪐
EOF
```

### 3. Paramètres SELinux
```bash=
cat << EOF > ./my-jupyterlab.te
module my-jupyterlab 1.0;

require {
        type init_t;
        type unconfined_exec_t;
        type user_home_t;
        type ephemeral_port_t;
        class tcp_socket { name_connect };
        class dir { create write read setattr rename };
        class file { append execute execute_no_trans ioctl map open read setattr create write relabelfrom rename };
        class lnk_file { getattr read };

}

#============= init_t ==============
allow init_t unconfined_exec_t:file { create execute open read };
allow init_t ephemeral_port_t:tcp_socket { name_connect };
allow init_t user_home_t:dir { create write read setattr rename };
allow init_t user_home_t:file { append execute execute_no_trans ioctl open read setattr map create write relabelfrom rename };
allow init_t user_home_t:lnk_file { getattr read };
EOF

checkmodule -M -m -o my-jupyterlab.mod my-jupyterlab.te

semodule_package -o my-jupyterlab.pp -m my-jupyterlab.mod

sudo semodule -i my-jupyterlab.pp
```

### 4. Création d'un service Jupyter Lab
* Remplacez: 🐱‍👤 par votre nom d'utilisateur et 🐱‍🐉 par le groupe de cet utilisateur.
```bash
cat << EOF | sudo tee /etc/systemd/system/jupyter.service
[Unit]
Description=Jupyter
[Service]
Type=simple
ExecStart=/home/🐱‍👤/jupyter/.venv/bin/jupyter-notebook
User=🐱‍👤
Group=🐱‍🐉
WorkingDirectory=/home/🐱‍👤/jupyter
Restart=always
RestartSec=10
[Install]
WantedBy=multi-user.target
EOF

sudo systemctl enable jupyter

sudo systemctl start jupyter
```

### 5. Configuration rapide du pare-feu (Config plus complète à la fin)
```bash
sudo firewall-cmd --add-port=8080/tcp --permanent
sudo firewall-cmd --reload
```

### Accès au site

Pour accéder au NoteBook Jupyter il vous suffit de vous rendre sur la page `http://<Votre_IP>:8080/lab?token=<votre_token>`.

### **➜ Avec un script d'install**

### script_jupyter.sh
```bash   
#!/bin/bash
# Installation of a Jupyter Notebook server 
# adrien~ 20/11/21

#on vérifie que le script est lancé en sudo
if [[ $(id -u) -ne 0 ]]; then
        echo "Sudo permission requiered"
        exit 1
fi

#On recupere le token donné sinon on met "jupyter"
token="jupyter"
if [[ ! -z $1 ]]; then
        token=$1
fi

#Installation des différents packages
dnf upgrade
dnf install python3
pip3 install --upgrade pip
pip3 install --upgrade setuptools
pip3 install --prefix /usr/local jupyter
dnf install checkpolicy

#On lance 2eme script avec l'utilisateur qui a appelé le sudo
su $SUDO_USER ./script_jupyter_user.sh $token

#On créé le fichier de notre service
cat << EOF | tee /etc/systemd/system/jupyter.service
[Unit]
Description=Jupyter
[Service]
Type=simple
ExecStart=/usr/local/bin/jupyter-notebook
User=$SUDO_USER
Group=$SUDO_USER
WorkingDirectory=/home/$SUDO_USER/jupyter
Restart=always
RestartSec=10
[Install]
WantedBy=multi-user.target
EOF

#On enable et start notre service
systemctl enable jupyter
systemctl start jupyter

#On ouvre le port 8080
firewall-cmd --add-port=8080/tcp --permanent
firewall-cmd --reload
```

### script_jupyter_user.sh
```bash
#!/bin/bash
# Installation of a Jupyter Notebook server 
# adrien~ 20/11/21

#On créé un dossier dans le home de notre utilisateur
mkdir -p ~/jupyter

#On ce deplace dans le dossier
cd ~/jupyter

#On install les différents module python qui pourront etre utilisé sur notre jupyter 
pip3 install --upgrade
pip3 install jupyterlab
pip3 install matplotlib
pip3 install pandas

#On créé un dossier dans le home de notre utilisateur
mkdir -p ~/.jupyter

#On créé un fichier de config de notre jupyter
cat << EOF > ~/.jupyter/jupyter_notebook_config.py
c.NotebookApp.allow_remote_access = True
c.NotebookApp.ip = '0.0.0.0'
c.NotebookApp.open_browser = False
c.NotebookApp.port = 8080
c.NotebookApp.token = '$1'
EOF

#Fichier qui s'executeras au lancement de notre service et qui servira a créer des fichiers de conf
cat << EOF > ~/my-jupyterlab.te
module my-jupyterlab 1.0;

require {
        type init_t;
        type unconfined_exec_t;
        type user_home_t;
        type ephemeral_port_t;
        class tcp_socket { name_connect };
        class dir { create write read setattr rename };
        class file { append execute execute_no_trans ioctl map open read setattr create write relabelfrom rename };
        class lnk_file { getattr read };

}

#============= init_t ==============
allow init_t unconfined_exec_t:file { create execute open read };
allow init_t ephemeral_port_t:tcp_socket { name_connect };
allow init_t user_home_t:dir { create write read setattr rename };
allow init_t user_home_t:file { append execute execute_no_trans ioctl open read setattr map create write relabelfrom rename };
allow init_t user_home_t:lnk_file { getattr read };
EOF

```

Pour installer le serveur jupyter notebook, il faut coller les 2 scripts dans le home de l'utilisateur qui executera le service.
Il faut lancer le script avec cet utilisateur (meme si le script sera lancé en root) avec par exemple la commande `sudo ./script_jupyter.sh`.
Vous pouvez ajouter 1 argument qui seras le token pour accéder au site web `sudo ./script_jupyter.sh jambon`

Il faudra à 2 reprise au début, accepter l'installation d'un package.

Lorsque que le script sera terminé, pour accéder au site il suffit d'aller sur la page `http://<Votre_IP>:8080/lab?token=<votre_token>` depuis un navigateur, le token est par défaut "jupyter".
Il faut bien sur ouvrir le port 8080, ce qui n'est pas fait automatiquement par le script par sécurité.

## Configuration avancée du serveur web

### Création et configuration d'un reverse proxy

#### 1. Installlation
```bash
sudo dnf install -y epel-release nginx
```

#### 2. Configuration
```bash 
# Dans votre /etc/hosts , ajoutez une ligne pour que votre serveur web ait un nom
sudo vim /etc/hosts
IP du serveur web      web.tp3.linux

sudo systemctl enable nginx # Pour qu'il démarre au boot 
# Modifiez votre conf nginx pour qu'elle ressemble à ça
cat /etc/nginx/nginx.conf
user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    include /etc/nginx/conf.d/*.conf;
}

# Puis créer un fichier dans /etc/nginx/conf.d/"n'importe quel nom".conf et mettez y le contenu suivant
sudo vim /etc/nginx/conf.d/web.tp3.linux.conf
server {
        listen 80;

        server_name web.tp3.linux;

        location / {
                proxy_pass http://web.tp3.linux:8080;
        }
}

# Démarrez nginx
sudo systemctl start nginx
```

### Https
```bash
# Génération du certificat avec OpenSSL
sudo openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
# Remplissez avec vos donnez (voici un exemple)
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:Nouvelle_Aquitaine
Locality Name (eg, city) [Default City]:Bordeaux
Organization Name (eg, company) [Default Company Ltd]: TestCompany
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server's hostname) []:web.tp3.linux
Email Address []: company@test.com

# Déplacez le certificat et la clé et changez les permissions sur ces fichiers
sudo mv server.crt /etc/pki/tls/certs/web.tp3.linux.crt
sudo chown root:root /etc/pki/tls/certs/web.tp3.linux.crt
sudo chmod 644 /etc/pki/tls/certs/web.tp3.linux.crt

sudo mv server.key /etc/pki/tls/private/web.tp3.linux.key
sudo chown root:root /etc/pki/tls/private/web.tp3.linux.key
sudo chmod 400 /etc/pki/tls/private/web.tp3.linux.key

# Enfin, modifiez la conf de votre reverse proxy
sudo vim /etc/nginx/conf.d/web.tp3.linux.conf
server {
        listen 443 ssl;
        ssl_certificate /etc/pki/tls/certs/web.tp3.linux.crt;
        ssl_certificate_key /etc/pki/tls/private/web.tp3.linux.key;
        server_name web.tp3.linux;

        location / {
                proxy_pass http://web.tp3.linux:8080;
        }
}

```

### Monitoring
```bash
# à faire sur les deux machines
sudo su -
bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
exit
```

### Firewall

* Sur le serveur web
```bash 
sudo firewall-cmd --set-default-zone=drop --permanent

sudo firewall-cmd --new-zone=ssh --permanent
sudo firewall-cmd --zone=ssh --permanent --add-source=#IP de votre machine dans le réseau du serveur
sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent

sudo firewall-cmd --new-zone=proxy --permanent
sudo firewall-cmd --zone=proxy --permanent --add-source=#IP du proxy
sudo firewall-cmd --zone=proxy --add-port=8080/tcp --permanent

sudo firewall-cmd --new-zone=netdata --permanent
sudo firewall-cmd --zone=netdata --permanent --add-source=#IP de votre machine dans le réseau du serveur
sudo firewall-cmd --zone=netdata --add-port=19999/tcp --permanent

```

* Sur le reverse proxy
```bash
sudo firewall-cmd --set-default-zone=drop

sudo firewall-cmd --new-zone=web --permanent
sudo firewall-cmd --zone=web --add-port=443/tcp --permanent

sudo firewall-cmd --new-zone=netdata --permanent
sudo firewall-cmd --zone=netdata --permanent --add-source=#IP de votre machine dans le réseau du serveur
sudo firewall-cmd --zone=netdata --add-port=19999/tcp --permanent
```
Notre solution n'utillisant pas de base de donnée mais de simple fichiers .ipynb, nous ne présentons pas de backup.
