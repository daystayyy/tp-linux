#!/bin/bash
# Installation of a Jupyter Notebook server 
# adrien~ 20/11/21

#on vérifie que le script est lancé en sudo
if [[ $(id -u) -ne 0 ]]; then
        echo "Sudo permission requiered"
        exit 1
fi

#On recupere le token donné sinon on met "jupyter"
token="jupyter"
if [[ ! -z $1 ]]; then
        token=$1
fi

#Installation des différents packages
dnf upgrade
dnf install python3
pip3 install --upgrade pip
pip3 install --upgrade setuptools
pip3 install --prefix /usr/local jupyter
dnf install checkpolicy

#On lance 2eme script avec l'utilisateur qui a appelé le sudo
su $SUDO_USER ./script_jupyter_user.sh $token

#On créé le fichier de notre service
cat << EOF | tee /etc/systemd/system/jupyter.service
[Unit]
Description=Jupyter
[Service]
Type=simple
ExecStart=/usr/local/bin/jupyter-notebook
User=$SUDO_USER
Group=$SUDO_USER
WorkingDirectory=/home/$SUDO_USER/jupyter
Restart=always
RestartSec=10
[Install]
WantedBy=multi-user.target
EOF

#On enable et start notre service
systemctl enable jupyter
systemctl start jupyter
