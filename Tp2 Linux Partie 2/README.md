# TP2 pt. 2 : Maintien en condition 
## I. Monitoring
### 2. Setup

Setup Netdata

On execute ces commandes sur nos 2 vms
```
# Passez en root pour cette opération
$ sudo su -

# Install de Netdata via le script officiel statique
$ bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)

# Quittez la session de root
$ exit
```

Manipulation du service Netdata

Le service est actif, et est paramétré pour démarrer au démarage de la machine
```
[adrien@web ~]$ systemctl is-active netdata
active
[adrien@web ~]$ systemctl is-enabled netdata
enabled
```

Le port sur lequel tourne Netdata
```
[adrien@web ~]$ sudo ss -lpnt
[sudo] password for adrien:
State     Recv-Q    Send-Q       Local Address:Port        Peer Address:Port    Process

[...]
LISTEN    0         128                0.0.0.0:19999            0.0.0.0:*        users:(("netdata",pid=2136,fd=5))
LISTEN    0         128                   [::]:19999               [::]:*        users:(("netdata",pid=2136,fd=6))
[...]

```
Netdata tourne sur le port 19999  
On ouvre le port , à faire sur les 2 machines
```
[adrien@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[adrien@web ~]$ sudo firewall-cmd --add-port=19999/tcp
success
```

Setup Alerting
On créé un webhook sur un serveur discord

Commande à faire sur les 2 vms

Modification du fichier 
```
[adrien@web netdata]$ sudo /opt/netdata/etc/netdata/edit-config health_alarm_notify.conf


###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="LINK"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="CHANNEL NAME"
```

DISCORD_WEBHOOK_URL="LINK" On remplace LINK par le lien de notre webhook, et CHANNEL NAME par le nom de notre channel discord

On restart netdata
```
[adrien@web ~]$ sudo systemctl restart netdata
```

Test des notifications 
```
# deviens l'utilisateur netdata
sudo su -s /bin/bash netdata

# active le debbugind sur la console
export NETDATA_ALARM_NOTIFY_DEBUG=1

# envoie une alarme test
/opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test
```

Config alerting



On tape cette commande pour l'alerting fonctionne correctement
```
sudo sed -i 's/curl=""/curl="\/opt\/netdata\/bin\/curl -k"/' /opt/netdata/etc/netdata/health_alarm_notify.conf
```

Création d'une nouvelle alerte pour recevoir une alerte à 50% de remplissage de la RAM

```
[adrien@web ~]$ sudo vim /opt/netdata/etc/netdata/health.d/ram-usage.conf       
 alarm: ram_usage
    on: system.ram
lookup: average -1m percentage of used
 units: %
 every: 1m
  warn: $this > 50
  crit: $this > 90
  info: The percentage of RAM being used by the system.
```

On restart netdata
```
[adrien@web ~]$ sudo systemctl restart netdata
```

On utilise la commande stress pour tester si l'alerte fonctionne
```
[adrien@web netdata]$ stress --vm 4 --vm-bytes 512           
```

On reçois notre notifications discord


## II. Backup

### 2. Partage NFS

Setup environnement
Création des différents dossiers 
```
[adrien@backup ~]$ sudo mkdir /srv/backups/
[adrien@backup ~]$ sudo mkdir /srv/backups/web.tp2.linux/   
[adrien@backup ~]$ sudo mkdir /srv/backups/db.tp2.linux/
```

Setup partage NFS
On install les packages nécessaire 
```
[adrien@backup ~]$ sudo dnf -y install nfs-utils
```

On modifie le fichier `/etc/idmapd.conf` on modifiant la ligne 5 en
```Domain = tp2.linux```

On modifie le fichier `/etc/exports` en ajoutant 
```
/srv/backups/web.tp2.linux/ 10.102.1.13/24(rw,no_root_squash)
/srv/backups/db.tp2.linux/ 10.102.1.13/24(rw,no_root_squash)
```

On lance et enable nfs-server et on ajoute nfs au firewall
```
[adrien@backup ~]$ sudo systemctl enable --now rpcbind nfs-server
[adrien@backup ~]$ sudo firewall-cmd --add-service=nfs
success
[adrien@backup ~]$ sudo firewall-cmd --add-service={nfs3,mountd,rpc-bind}
success
[adrien@backup ~]$ sudo firewall-cmd --runtime-to-permanent
success
```

Setup points de montage sur web.tp2.linux

On install les packages nécessaire 
```
[adrien@web ~]$ sudo dnf -y install nfs-utils
```
On modifie le fichier `/etc/idmapd.conf` on modifiant la ligne 5 en
```Domain = tp2.linux```

On monte le dossier 
```
[adrien@web ~]$ sudo mount -t nfs backup.tp2.linux:/srv/backups/web.tp2.linux/ /srv/backup/
```

On vérifie qu'il reste de la place 
```
[adrien@web ~]$ df -h
Filesystem                                   Size  Used Avail Use% Mounted on
[...]
backup.tp2.linux:/srv/backups/web.tp2.linux  6.2G  2.2G  4.1G  36% /srv/backup
```
On a 4.1G de libre

On a le droit d'écrire dans la partition
```
[adrien@web ~]$ sudo touch /srv/backup/
```

La partition se monte automatiquement
```
[adrien@web ~]$ cat /etc/fstab
[...]
backup.tp2.linux:/srv/backups/web.tp2.linux/ /srv/backup/       nfs     default    0 0
```

### 3. Backup de fichiers

J'ai créé un dossier `destination` et un dossier `testFolder` qui contient un fichier bonjour.txt, pour les tests 

```
[adrien@web srv]$ cat testFolder/bonjour.txt
SALUT
```

On test 
```
[adrien@web srv]$ sudo ./tp2_backup.sh destination/ testFolder/
The archive tp2_backup_211025_102533.tar.gz was created
The archive tp2_backup_211025_102533.tar.gz was move to destination/
[OK] Directory destination/ cleaned to keep only the 5 most recent backups.

[adrien@web srv]$ ls destination/
tp2_backup_211025_101622.tar.gz  tp2_backup_211025_102319.tar.gz  tp2_backup_211025_102533.tar.gz
tp2_backup_211025_102317.tar.gz  tp2_backup_211025_102320.tar.gz
```

On décompresse le fichier
```
[adrien@web srv]$ sudo tar -xzvf destination/tp2_backup_211025_102533.tar.gz -C destination/
testFolder/
testFolder/bonjour.txt

[adrien@web srv]$ cat destination/testFolder/bonjour.txt
SALUT
```

### 4. Unité de service

### A. Unité de service

```
[adrien@web ~]$ cat /etc/systemd/system/tp2_backup.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/destination /srv/testFolder
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```
Tester le bon fonctionnement
```
[adrien@web srv]$ sudo systemctl daemon-reload
[adrien@web srv]$ sudo systemctl start tp2_backup.service
[adrien@web srv]$ ls /srv/destination/
testFolder                       tp2_backup_211025_102319.tar.gz  tp2_backup_211025_102533.tar.gz
tp2_backup_211025_102317.tar.gz  tp2_backup_211025_102320.tar.gz  tp2_backup_211025_102948.tar.gz
```

### B. Timer

```
[adrien@web ~]$ sudo cat /etc/systemd/system/tp2_backup.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* *:*:00

[Install]
WantedBy=timers.target
```

Activez le timer

```
[adrien@web ~]$ sudo systemctl start tp2_backup.timer
[adrien@web ~]$ sudo systemctl enable tp2_backup.timer
Created symlink /etc/systemd/system/timers.target.wants/tp2_backup.timer → /etc/systemd/system/tp2_backup.timer.

[adrien@web ~]$ sudo systemctl is-active tp2_backup.timer
active

[adrien@web ~]$ sudo systemctl is-enabled tp2_backup.timer
enabled
```

Tests !
```
[adrien@web srv]$ ls destination/
testFolder                       tp2_backup_211025_103805.tar.gz  tp2_backup_211025_104005.tar.gz
tp2_backup_211025_103705.tar.gz  tp2_backup_211025_103905.tar.gz  tp2_backup_211025_104105.tar.gz
```
Avec les fichiers suivant on vois les minutes dans leur noms et on remarque que la sauvegarde ce fait bien toutes les minutes
tp2_backup_211025_10**37**05.tar.gz 
tp2_backup_211025_10**38**05.tar.gz 
tp2_backup_211025_10**39**05.tar.gz  tp2_backup_211025_10**40**05.tar.gz
tp2_backup_211025_10**41**05.tar.gz


### C. Contexte

```
[adrien@web ~]$ cat /etc/systemd/system/tp2_backup.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/backup /var/www/sub-domains/web.tp2.linux/html/nextcloud
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

```
[adrien@web ~]$ cat /etc/systemd/system/tp2_backup.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* 3:15:00

[Install]
WantedBy=timers.target
```

Le service va bien s'exécuter la prochaine fois qu'il sera 03h15
```
[adrien@web ~]$ sudo systemctl list-timers
NEXT                          LEFT          LAST                          PASSE>
[...]
Tue 2021-10-26 03:15:00 CEST  16h left      n/a                           n/a          tp2_backup.timer             tp2_backup.service
[...]
3 timers listed.
Pass --all to see loaded but inactive timers, too.
```
Tue 2021-10-26 **03:15:00**

### 5. Backup de base de données

On test le script 
```
[adrien@db srv]$ sudo ./tp2_backup_db.sh testFolder/ nextcloud
Database is copied
The archive tp2_backup_db_211025_113628.tar.gz was created
The archive tp2_backup_db_211025_113628.tar.gz was move to testFolder/
[OK] Directory testFolder/ cleaned to keep only the 5 most recent backups.

[adrien@db srv]$ ls testFolder/
tp2_backup_db_211025_113519.tar.gz  tp2_backup_db_211025_113628.tar.gz


[adrien@db srv]$ cd testFolder/
[adrien@db testFolder]$ sudo tar -xzvf tp2_backup_db_211025_113628.tar.gz
nextcloud.sql
[adrien@db testFolder]$ mysql -h localhost -u root -p adrien nextcloudTest < nextcloud.sql
```

Unité de service
```
[adrien@db backup]$ sudo cat /etc/systemd/system/tp2_backup_db.service
[Unit]
Description=Our own lil backup db service (TP2)

[Service]
ExecStart=/srv/tp2_backup_db.sh /srv/backup nextcloud
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```
```
[adrien@db backup]$ sudo cat /etc/systemd/system/tp2_backup_db.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup_db.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* 3:30:00

[Install]
WantedBy=timers.target
```
J'ai une erreur lorsque je start mon service
```
[adrien@db backup]$ sudo journalctl -xe
[...]
Oct 25 12:07:43 db.tp2.linux systemd[2890]: tp2_backup_db.service: Failed to execute command: Permission denied
Oct 25 12:07:43 db.tp2.linux systemd[2890]: tp2_backup_db.service: Failed at step EXEC spawning /srv/tp2_backup_db.sh: Permission denied
[...]
```


## III. Reverse Proxy

### 2. Setup simple

Installer NGINX

```
[adrien@front ~]$ sudo dnf install epel-release
[...]
[adrien@front ~]$ sudo dnf upgrade
[...]
[adrien@front ~]$ sudo dnf install nginx
[...]
```

Tester !

On start et enable nginx
```
[adrien@front ~]$ sudo systemctl start nginx
[adrien@front ~]$ sudo systemctl enable nginx
```


Le port utilisé par ngnix est le port 80
```
[adrien@front ~]$ sudo ss -l -p -n -t
[...]
State       Recv-Q      Send-Q           Local Address:Port            Peer Address:Port      Process
LISTEN      0           128                    0.0.0.0:80                   0.0.0.0:*          users:(("nginx",pid=16182,fd=8),("nginx",pid=16181,fd=8))
[...]
LISTEN      0           128                       [::]:80                      [::]:*          users:(("nginx",pid=16182,fd=9),("nginx",pid=16181,fd=9))
[...]
```

On ouvre le port 80
```
[adrien@front ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[adrien@front ~]$ sudo firewall-cmd --add-port=80/tcp
success

```

Curl depuis mon pc
```
C:\Users\adrie>curl front.tp2.linux
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
[...]
  </body>
</html>
```

Explorer la conf par défaut de NGINX

L'utilisateur utilisé par NGINX est nginx
```
[adrien@front ~]$ cat /etc/nginx/nginx.conf
[...]
user nginx;
[...]
```

Block server{}
```
    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
```

Inclusions des autres fichiers de conf
```
[adrien@front ~]$ cat /etc/nginx/nginx.conf
[...]
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;
[...]
include /usr/share/nginx/modules/*.conf;
[...]
    access_log  /var/log/nginx/access.log  main;
[...]

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

[...]
    include /etc/nginx/conf.d/*.conf;
[...]
```


Modifier la conf de NGINX

On supprime le bloc serveur et on créé le fichier `/etc/nginx/conf.d/web.tp2.linux.conf`
```
[adrien@front ~]$ sudo cat /etc/nginx/conf.d/web.tp2.linux.conf
server {
    listen 80;
    server_name web.tp2.linux;
    location / {
        proxy_pass http://web.tp2.linux;
    }
}
```

## IV. Firewalling
### 2. Mise en place
#### A. Base de données

Restreindre l'accès à la base de données db.tp2.linux
```
[adrien@db ~]$ sudo firewall-cmd --set-default-zone=drop
success
[adrien@db ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8
Warning: ZONE_ALREADY_SET: 'enp0s8' already bound to 'drop'
success

[adrien@db ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8 --permanent
The interface is under control of NetworkManager, setting zone to 'drop'.
success

[adrien@db ~]$ sudo firewall-cmd --new-zone=db --permanent
success
[adrien@db ~]$ sudo firewall-cmd --new-zone=ssh --permanent
success

[adrien@db ~]$ sudo firewall-cmd --reload
success

[adrien@db ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp
success
[adrien@db ~]$ sudo firewall-cmd --zone=db --add-port=3306/tcp
success
[adrien@db ~]$ sudo firewall-cmd --zone=db --add-port=3306/tcp --permanent
success
[adrien@db ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
success

[adrien@db ~]$ sudo firewall-cmd --zone=db --add-source=10.102.1.11/32
success
[adrien@db ~]$ sudo firewall-cmd --zone=db --add-source=10.102.1.11/32 --permanent
success

[adrien@db ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
success
[adrien@db ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32
success
```
Montrez le résultat de votre conf avec une ou plusieurs commandes
```
[adrien@db ~]$ sudo firewall-cmd --get-active-zones
db
  sources: 10.102.1.11/32
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/32
  
[adrien@db ~]$ sudo firewall-cmd --get-default-zone
drop

[adrien@db ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  
[adrien@db ~]$ sudo firewall-cmd --list-all --zone=db
db (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/32
  services:
  ports: 3306/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

```
#### B. Serveur Web

Restreindre l'accès au serveur Web web.tp2.linux
Meme procédure qu'avec db.tp2.linux avec des ip et ports différents 

Montrez le résultat de votre conf
```
[adrien@web ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
front
  sources: 10.102.1.14/32
ssh
  sources: 10.102.1.1/32
  
[adrien@web ~]$ sudo firewall-cmd --get-default-zone
drop

[adrien@web ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  
[adrien@web ~]$ sudo firewall-cmd --list-all --zone=front
front (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.14/32
  services:
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

#### C. Serveur de backup
Restreindre l'accès au serveur de backup backup.tp2.linux
Meme procédure qu'avec db.tp2.linux avec des ip et ports différents 

Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd

```
[adrien@backup ~]$ sudo firewall-cmd --get-active-zones
backup
  sources: 10.102.1.11/32 10.102.1.12/32
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/32
  
[adrien@backup ~]$ sudo firewall-cmd --get-default-zone
drop

[adrien@backup ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  
[adrien@backup ~]$ sudo firewall-cmd --list-all --zone=backup
backup (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/32 10.102.1.12/32
  services:
  ports: 2049/tcp 111/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```


#### D. Reverse 

```
[adrien@front ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
proxy
  sources: 10.102.1.0/24
ssh
  sources: 10.102.1.1/32
  
[adrien@front ~]$ sudo firewall-cmd --get-default-zone
drop


[adrien@front ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  
[adrien@front ~]$ sudo firewall-cmd --list-all --zone=proxy
proxy (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.0/24
  services:
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

#### E. Tableau récap


| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | `80/tcp, 22/tcp `          | `10.102.1.14\32`             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | `3306/tcp, 22/tcp`           | `10.102.1.11\32`             |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | `2049/tcp, 111/tcp, 22/tcp`           |`10.102.1.11\32, 10.102.1.12\32`             |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | `80/tcp, 22/tcp`          | `10.102.1.0\24`            |
