#!/bin/bash
# Simple backup db script
# adrien~ 25/10/21

# Usage
usage() {
    echo "Usage: $0 <destination> <database_name>"
}

if [[ -z $1 || -z $2 ]] ; then
  >&2 echo -e "${R}${B}[ERROR]${N} You must specify a destination and a database name."
  >&2 echo
  usage
fi

# Global vars
destination=$1
database_name=$2
backup_name=$(date +tp2_backup_db_%y%m%d_%H%M%S.tar.gz)
backup_fullpath="$(pwd)/${backup_name}"


# Check
if [[ ! -d $destination ]] ; then
  >&2 echo "The destination \"${destination}\" is not accessible."
  exit 1
fi

if ! mysql -u root --password=adrien ${database_name} -e exit &> /dev/null ; then
  >&2 echo -e "The database ${database_name} does not exist."
  exit 1
fi



# copy database to pwd
mysqldump -u root --password=adrien --databases ${database_name} > ${database_name}.sql 1> /dev/null
if [[ $? -eq 0 ]] ; then
  echo "Database is copied"
else
  >&2 echo "Failed copied database,look at the error message above"
  exit 1
fi

# Move archive to destination
tar cvzf "${backup_fullpath}" "${database_name}.sql" 1> /dev/null && rm ${database_name}.sql 1> /dev/null
if [[ $? -eq 0 ]] ; then
  echo ""The archive ${backup_name} was created""
else
  >&2 echo "Error : Failed during archiving, look at the error message above"
  exit 1
fi


# Move archive to destination
rsync -av --remove-source-files "${backup_fullpath}" "${destination}" 1> /dev/null
if [[ $? -eq 0 ]] ; then
  echo "The archive ${backup_name} was move to ${destination}"
else
  >&2 echo -e ""
  exit 1
fi


clean_backup() {
  # Keep 5 backups by default
  if [[ -z $qty_to_keep ]] ; then qty_to_keep=5 ; fi

  qty_to_keep_tail=$((qty_to_keep+1))
  ls -tp "${destination}" | grep -v '/$' | tail -n +${qty_to_keep_tail} | xargs -I {} rm -- ${destination}/{}
  status=$?
  if [[ $status -eq 0 ]] ; then
    echo -e "${G}${B}[OK]${N} Directory ${destination} cleaned to keep only the ${qty_to_keep} most recent backups."
  else
    >&2 echo -e "${R}${B}[ERROR]${N} Failed to clean ${destination}. Old backups may remain."
    exit 1
  fi
}


clean_backup 5
