#!/bin/bash
# Simple backup script
# Adrien ~ 25/10/21

# Usage
usage() {
    echo "Usage: $0 <destination> <dir_to_backup>"
}

if [[ -z $1 || -z $2 ]] ; then
  >&2 echo -e "You need to specify a destination and a directory to backup."
  >&2 echo
  usage
fi

# Global vars
destination=$1
dir_to_backup=$2
backup_name=$(date +tp2_backup_%y%m%d_%H%M%S.tar.gz)
backup_fullpath="$(pwd)/${backup_name}"

# Check directory
if [[ ! -d $destination ]] ; then
  >&2 echo "The destination \"${destination}\" is not accessible."
  exit 1
fi

if [[ ! -d $dir_to_backup ]] ; then
  >&2 echo "The directory to backup \"${dir_to_backup}\" is not accessible."
  exit 1
fi

# Create archive of dir_to_backup
tar cvzf "${backup_fullpath}" "${dir_to_backup}" 1> /dev/null
if [[ $? -eq 0 ]] ; then
  echo "The archive ${backup_name} was created"
else
  >&2 echo
  >&2 echo "Error : Failed during archiving, look at the error message above"
  exit 1
fi


# Move archive to destination
rsync -av --remove-source-files "${backup_fullpath}" "${destination}" 1> /dev/null
if [[ $? -eq 0 ]] ; then
  echo "The archive ${backup_name} was move to ${destination}"
else
  >&2 echo
  >&2 echo "Error : Failed moving, look at the error message above"
  exit 1
fi

clean_backup() {
  # Keep 5 backups by default
  if [[ -z $qty_to_keep ]] ; then qty_to_keep=5 ; fi

  qty_to_keep_tail=$((qty_to_keep+1))
  ls -tp "${destination}" | grep -v '/$' | tail -n +${qty_to_keep_tail} | xargs -I {} rm -- ${destination}/{}
  status=$?
  if [[ $status -eq 0 ]] ; then
    echo -e "${G}${B}[OK]${N} Directory ${destination} cleaned to keep only the ${qty_to_keep} most recent backups."
  else
    >&2 echo -e "${R}${B}[ERROR]${N} Failed to clean ${destination}. Old backups may remain."
    exit 1
  fi
}

clean_backup 5
